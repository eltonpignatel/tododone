import React, {Component} from 'react';
import './Navbar.css';

class Navbar extends Component {
  
  handleIncuirItem() {
    this.props.handlerNavbar({
      tipo:"item", 
      quadro:"A Fazer", 
      item:"Item Novo"
    });
  }

  handleIncuirQuadro() {
    this.props.handlerNavbar({
      tipo:"quadro", 
      quadro:"Excluido"
    });
  }

  render() {
    return (
      <header>
        <h2><button>Painel de to-dos</button></h2>
        <nav>
          <li><button onClick={() => this.handleIncuirItem()}>Incluir Item</button></li>
          <li><button onClick={() => this.handleIncuirQuadro()}>Incluir Quadro</button></li>
          <li><button onClick={() => alert('Não tem nada aqui')}>About</button></li>
          <li><button onClick={() => alert('Não tem nada aqui')}>Contato</button></li>
        </nav>
      </header>
    );
  }
}

export default Navbar;