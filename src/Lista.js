import React, {Component} from 'react';
import './Lista.css';

class Lista extends Component {

    handleIncluirItem(valorItem) {
        this.props.handlerLista({tipo:"incluirItem", quadro:"", item:valorItem});
    }

    render() {
        const conteudoLista = this.props.descricao.map((conteudo, index) => (
            <li className="ItemLista" key={index}>{conteudo}<button className="ApagarItem" title="Apagar item">X</button></li>
        ));
        return(
            <div>
                <ul className="Lista">
                    {conteudoLista}
                </ul>
                <input className="IncluirItem" onDoubleClick={(evt) => this.handleIncluirItem(evt.target.value)}/>
                
            </div>
        );
    }
}

export default Lista;