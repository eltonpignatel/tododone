import React, {Component} from 'react';
import './App.css';
import QuadroEtapa from './QuadroEtapa'
import Navbar from './Navbar';

class App extends Component {
  constructor(props){

    super(props);

    this.handlerNavbar = this.handlerNavbar.bind(this);
    this.handlerQuadroEtapa = this.handlerQuadroEtapa.bind(this);

    this.state = {
      quadros:[
        {
        titulo:"A Fazer",
        lista:["Nada",
              "Item 2",
              "Item 33"] },

        {
          titulo:"Fazendo",
          lista:["Item 1",
                "Item 2",
                "Item 3",
                "Item 4"] }, 

        {
          titulo:"Feito",
          lista:["Item 1",
                "Item 2",
                "Item 3",
                "Item 4",
                "Item 5"] },
        {
          titulo:"Outro",
          lista:["Item 1",
                "Item 2",
                "Item 3",
                "Item 4",
                "Item 5"] }
      ]
    }
  }

  handlerNavbar(value) {

    let newState = this.state;
    if (value.tipo === "item" ) {

      newState.quadros[0].lista.push(value.item);
      this.setState(newState);

    } else if(value.tipo === "quadro" ) {

      newState.quadros.push({titulo:value.quadro,lista:[]});
      this.setState(newState);

    }

  }

  handlerQuadroEtapa(value) {

    if (value.tipo === "excluirQuadro") {

      let newState = {quadros: this.state.quadros.filter((quadro, index) => {
          return (quadro.titulo !== value.titulo);
      }) }; 

      this.setState(newState);

    } else if (value.tipo === "incluirItem") {
      
      let newState = {quadros: this.state.quadros.map((quadro, index) => {
        if (quadro.titulo === value.quadro) {
          quadro.lista.push(value.item);
          return quadro;
        } else {
          return quadro;
        }
      }) };

      this.setState(newState);
    }
    
  }

  render() {

    return (
      <div className="tododone-container">
        <Navbar  handlerNavbar={this.handlerNavbar} />
        <div className="quadros">
          {
            this.state.quadros.map((quadro, index) => (
              <QuadroEtapa key={index} {...quadro} handlerQuadroEtapa={this.handlerQuadroEtapa} />
            ))
          }
        </div>
        
      </div>
    );
  }
}

export default App;