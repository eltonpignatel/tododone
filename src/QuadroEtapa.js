import React, {Component} from 'react';
import Lista from './Lista';
import './QuadroEtapa.css';

class QuadroEtapa extends Component {

    constructor(props){

        super(props);

        this.handlerLista = this.handlerLista.bind(this);
    }

    handleApagarQuadro(titulo) {
        this.props.handlerQuadroEtapa({tipo:"excluirQuadro", titulo});
    }

    handlerLista(value) {
        this.props.handlerQuadroEtapa({tipo:"incluirItem", quadro:this.props.titulo, item:value.item});
    }

    render() {
        const {titulo, lista} = this.props;

        return (
            <div className="QuadroEtapa" >
                <div className="botaoExcluir" title="Excluir" onClick={() => this.handleApagarQuadro(titulo)}/>
                <div className="titulo">
                    <h1>{titulo}</h1>
                </div>
                <Lista descricao = {lista} handlerLista={this.handlerLista}/>
            </div>
        );
    }
}

export default QuadroEtapa;