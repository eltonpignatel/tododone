/********************************************************************************
Compilado das coisas que vou aprendendo
********************************************************************************/
*Criação da aplicação
-Opção 1: => https://reactjs.org/docs/add-react-to-a-website.html
-Criar manualmente incluindo os scrips na página e usando javascrip puro.
-Há a opção de usar ou não jsx usando babel para transpilar.
-Opção 2(recomendado): => https://reactjs.org/docs/create-a-new-react-app.html
-Instalação do nodejs.
-Criação da aplicação com comando npx create-react-app my-app. 
-Iniciar aplicação com npm start(Quando importei o projeto do git em outra máquina tive que rodar npm install antes para instalar a pasta node_modules)
-Esta aplicação já inclui vários pacotes contidos e mantidos pelo npm nodejs, 
é controlada pelo arquivo package.json e pode ser alterada e atualizada via npm install.
-A ferramenta de debug Chrome Debug Extension permite debugar o programa no pŕoprio VSCode
-Encontrei várias coisas legais neste link: https://github.com/rithmschool/udemy_course_exercises/tree/master/react/recipe-props-solution
--------------------------------------
Sintaxe básica da aplicação:
-O index.js apenas inclui a aplicação principal (pode ser App.js) no root do index.html
-Importar componentes react do node_modules
-Importar arquivos necessários como os CSS 
-Criar classe estendendo Component 
-Criar método render e colocar o JSX dentro do return

Conceito de Props
-Props são as propriedades dos componentes HTML customizados
-Props são imutáveis
-Há algumas sintaxes javascript importantes, como o map(r,i) => (), a inicialização de variáveis por {var1, var2} = this.props;
-Há recursos importantes do react para tratramento de props, como os defaultProps e PropTypes

Conceito de state
-É o estado da aplicação
-Não pode ser alterado diretamente, deve ser passado outro objeto por set state
-Deve ser definido no componente pai, e é automaticamente repassado via parametro aos componentes 
-Nunca alterar o state no construtor dos parametros em componentes filhos
-O state deve ter o mínimo para conter o ESTADO da aplicação, e não todos os dados(ler https://reactjs.org/docs/thinking-in-react.html)